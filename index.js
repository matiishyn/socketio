var express = require('express'),
    app = express(),
    http = require('http').Server(app),
    io = require('socket.io')(http);


app.use(express.static('./web'))
    .get('/', function (req, resp) {
        resp.sendFile(__dirname + '/web/index.html');
    });
http.listen(3000, function () {
    console.log('listening on 3000');
});

io.on('connection', function (socket) {
    console.log('connetcted');
    socket.broadcast.emit('hi');
    socket.on('disconnect', function () {
        console.log('user disconected');
    });
    socket.on('chat message', function (msg) {
        console.log("Message: ", msg);
        io.emit('chat message', msg);
    });
});